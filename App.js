import React from 'react';
import Route from './src/Route/Route';
import storeRedux from './src/redux/Store.js';
import { Provider } from 'react-redux';

const App = () => {
  return (
    <Provider store={storeRedux}>
      <Route />
    </Provider>
  );
};

export default App;
