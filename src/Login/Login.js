import { View, Text, StyleSheet, SafeAreaView, ActivityIndicator } from 'react-native'
import React, { useEffect, useState } from 'react'
import colors from '../Helper/colors'
import { responsiveHeight } from '../Helper/Responsive'
import { GenerateItems, GeneratedToken } from '../redux/Action/Action'
import { useDispatch, useSelector } from 'react-redux'

const Login = () => {

    const dispatch = useDispatch()
    function hitToken() {
        try {
            dispatch(GeneratedToken())
            dispatch(GenerateItems())
        } catch (e) {
            console.log(e)
        }
    }
    useEffect(() => {
        setTimeout(() => {
            hitToken()
        }, 4000);
    }, [])


    return (
        <SafeAreaView style={style.container}>
            <View style={style.containerBox}>
                <ActivityIndicator color={colors.PrimaryWhite} animating={true} size={responsiveHeight(50)} />
                <Text style={style.textStyle}>Loading Token</Text>
            </View>
        </SafeAreaView>
    )
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.Primary,
        justifyContent: 'center',
        alignItems: 'center'
    },
    containerBox: {
        backgroundColor: 'rgba(33, 33, 33, 0.75)',
        height: responsiveHeight(150),
        width: responsiveHeight(150),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
    },
    textStyle: {
        color: colors.PrimaryWhite,
        fontSize: responsiveHeight(16),
        marginTop: responsiveHeight(10)
    }
})
export default Login