import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";

export const logout = () => ({
    type: 'LOGOUT',
});

export const GeneratedToken = () => {
    return async dispatch => {
        const dataToken = async (value) => {
            await AsyncStorage.setItem('Token', value)
        }
        const data = {
            grant_type: 'client_credentials',
            client_id: 'profes-api',
            client_secret: 'P@ssw0rd',
        };
        var config = {
            method: 'post',
            url: 'https://dev.profescipta.co.id/so-api/token',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: data
        };

        axios(config)
            .then(function (response) {
                const data = response.data
                dispatch({
                    type: 'LOGIN_SUCCESS',
                    data: data,
                })
                dataToken(response.data.access_token)
                setTimeout(() => {
                    dispatch(logout())
                }, response.data.expires_in * 100);
            })
            .catch(function (error) {
                console.log(error);
            });
    }

}

export const GenerateItems = () => {
    return async dispatch => {
        const token = await AsyncStorage.getItem('Token')
        if (token !== null) {
            var config = {
                method: 'get',
                url: 'https://dev.profescipta.co.id/so-api/Order/GetItems',
                headers: {
                    'state': '12345',
                    'Authorization': `Bearer ${token}`
                }
            };

            axios(config)
                .then(function (response) {
                    console.log(JSON.stringify(response.data), 'data');
                    const data = response.data
                    dispatch({
                        type: 'GET_DATA',
                        data: data,
                    })
                })
                .catch(function (error) {
                    console.log(error);
                });

        }
    }
}


export const Deteles = (ids) => {
    return async dispatch => {
        const token = await AsyncStorage.getItem('Token')
        var data = JSON.stringify({
            "ItemId": ids
        });

        var config = {
            method: 'post',
            url: 'https://dev.profescipta.co.id/so-api/Order/DeleteItem',
            headers: {
                'state': '12345',
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            data: data
        };

        axios(config)
            .then(function (response) {
                console.log(JSON.stringify(response.data), 'datadel');
                dispatch(GenerateItems())
            })
            .catch(function (error) {
                console.log(error);
            });
    }
}