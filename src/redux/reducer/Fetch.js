const initialState = {
    isLoading: false,
    ListItems: null,
    ListOrder: null
}
const Fetch = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_DATA':
            return {
                ...state,
                loading: true,
                ListItems: action.data
            };
        case 'GET_DATA_ORDER':
            return {
                ...state,
                loading: true,
                ListOrder: action.data
            };
        default:
            return state;
    }
}

export default Fetch;