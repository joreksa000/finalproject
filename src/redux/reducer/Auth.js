const initialState = {
    isLoading: false,
    AuthData: null
}
const Auth = (state = initialState, action) => {
    switch (action.type) {
        case 'LOGIN':
            return {
                ...state,
                isLoading: true,
            };
        case 'LOGIN_SUCCESS':
            return {
                ...state,
                isLoading: true,
                AuthData: action.data
            };
        case 'REGISTER':
            return {
                ...state,
                AuthData: action.data
            }
        case 'LOGOUT':
            return {
                ...state,
                isLoading: true,
                AuthData: null,
            };
        default:
            return state;
    }
}

export default Auth;