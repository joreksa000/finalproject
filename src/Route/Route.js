import React, { useEffect, useState } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { useSelector } from 'react-redux';
import Home from '../Home/Home';
import Login from '../Login/Login';
import ListOrders from '../Home/ListOrders';
// import AsyncStorage from '@react-native-async-storage/async-storage';

const AppStack = () => {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="List" component={ListOrders} />
        </Stack.Navigator>
    );
};

const AuthStack = () => {
    return (
        <Stack.Navigator screenOptions={{ headerShown: false }}>
            <Stack.Screen name="Login" component={Login} />
        </Stack.Navigator>
    );
};


const Stack = createNativeStackNavigator();

const Route = () => {
    const [tokens, setTokens] = useState(null)
    const token = useSelector(state => state.auth.AuthData)

    const funcsToken = async () => {
        // const token = await AsyncStorage.getItem('Token')
        setTokens(token)
        console.log('Tokens', token)
    }

    useEffect(() => {
        funcsToken()
    }, [token])

    return (
        <NavigationContainer>
            {tokens !== null ? <AppStack /> : <AuthStack />}
        </NavigationContainer>

    )
}

export default Route