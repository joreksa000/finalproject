import { View, Text, StyleSheet, SafeAreaView, Image, TouchableOpacity, TextInput, FlatList } from 'react-native'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import colors from '../Helper/colors'
import { responsiveHeight } from '../Helper/Responsive'
import ButtonPrimary from '../Component/ButtonPrimary'
import Cards from '../Component/Cards'
import numberWithCommas from '../Helper/Formater'
import Modals from '../Component/Modal'


const Home = ({ navigation }) => {

    const [salesNum, setSalesNum] = useState('')
    const [salesDate, setSalesDate] = useState('')
    const [costumer, setCostumer] = useState('')
    const [Address, setAddress] = useState('')
    const [totalPrice, setTotalPrice] = useState(0)
    const [totalQuantity, setTotalQuantity] = useState(null)
    const [posib, setPosib] = useState(false)
    const dataList = useSelector(state => state.fetch?.ListItems) || ''


    useEffect(() => {
        if (dataList !== '') {
            cekker()
        }
    }, [])




    const cekker = () => {
        const totalPrices = Object?.values(dataList)?.reduce((total, item) => total + (item.Price * item.Quantity), 0);
        setTotalPrice(totalPrices)
        const totalQuantitys = Object?.values(dataList)?.reduce((total, item) => total + item.Quantity, 0);
        setTotalQuantity(totalQuantitys)
    }



    return (
        <SafeAreaView style={style.container}>
            <View style={style.containerPd}>
                <View style={style.containerImage}>
                    <Image source={{ uri: 'https://cdn-icons-png.flaticon.com/512/6596/6596121.png' }} style={style.imageStyle} />
                    <TouchableOpacity>
                        <Image source={{ uri: 'https://assets.stickpng.com/images/588a64e0d06f6719692a2d10.png' }} style={style.menuStyle} />
                    </TouchableOpacity>
                </View>
                <View style={style.HeadingContainer}>
                    <Text style={style.headingStyle}>SALES ORDER INPUT</Text>
                </View>
            </View>
            <View style={style.canvas}>
                <View style={style.padss}>
                    <View style={style.styleInformant}>
                        <View>
                            <Text style={style.salesStyle}>Sales Information</Text>
                        </View>
                        <View style={style.textInputBarSm}>
                            <TextInput
                                placeholder='Sales Order Number'
                                style={style.textInputStyleSM}
                                value={salesNum}
                                onChangeText={(e) => {
                                    setSalesNum(e)
                                }}
                            />
                        </View>
                        <View style={style.textInputBarSm}>
                            <TextInput
                                placeholder='Sales Order Date'
                                style={style.textInputStyleSM}
                                value={salesDate}
                                onChangeText={(e) => {
                                    setSalesDate(e)
                                }}
                                keyboardType='numeric'
                            />
                        </View>
                        <View style={style.textInputBarSm}>
                            <TextInput
                                placeholder='costumer'
                                style={style.textInputStyleSM}
                                value={costumer}
                                onChangeText={(e) => {
                                    setCostumer(e)
                                }}
                            />
                        </View>
                        <View style={style.textInputBarBig}>
                            <TextInput
                                placeholder='Address'
                                style={style.textInputStyleBig}
                                value={Address}
                                onChangeText={(e) => {
                                    setAddress(e)
                                }}
                            />
                        </View>
                    </View>
                    <View style={style.styleHeading2}>
                        <Text style={style.textDetailStyle}>Detail Sales</Text>
                        <ButtonPrimary
                            Label={'Add Items'}
                            onpress={() => {
                                setPosib(!posib)
                            }} />
                    </View>
                    <View style={style.cons}>
                        <FlatList
                            data={dataList}
                            renderItem={values => (
                                <View style={[style.cons, { marginVertical: responsiveHeight(5) }]}>
                                    <Cards types={'Sale'} data={values.item} updates={() => {
                                        setPosib(!posib)
                                    }} />
                                </View>
                            )}
                        />
                    </View>
                </View>
                <View style={style.contBot}>
                    <View style={{ marginHorizontal: responsiveHeight(10), marginVertical: responsiveHeight(5) }}>
                        <Text>Order Sumarry</Text>
                    </View>
                    <View style={{ marginHorizontal: responsiveHeight(20) }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <Text>SubTotal</Text>
                            <Text>RP {numberWithCommas(totalPrice)}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <Text>Total Product</Text>
                            <Text>{totalQuantity} Product</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: responsiveHeight(10) }}>
                        <ButtonPrimary
                            Label={'Process Order'}
                            widths={100}
                            onpress={() => {
                                navigation.navigate('List')
                            }} />
                        <View style={{ marginHorizontal: responsiveHeight(20), }} />
                        <ButtonPrimary
                            Label={'cancel'}
                            colorss={'white'}
                            widths={100}
                            bodco={'ada'}
                            onpress={() => {
                                console.log('1')
                            }} />
                    </View>
                </View>
            </View>
            <Modals visible={posib} onpress={() => {
                setPosib(!posib)
            }} />
        </SafeAreaView >
    )
}


const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.Primary,
    },
    textDetailStyle: {
        color: 'black',
        fontSize: responsiveHeight(18),
        fontWeight: '600'
    },
    styleHeading2: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginVertical: responsiveHeight(20)
    },
    textInputBarBig: {
        borderWidth: 1,
        paddingHorizontal: responsiveHeight(21),
        borderRadius: 10,
        marginBottom: responsiveHeight(10)
    },
    textInputBarSm: {
        borderWidth: 1,
        paddingHorizontal: responsiveHeight(21),
        justifyContent: 'center',
        borderRadius: 10,
    },
    textInputStyleSM: {
        height: responsiveHeight(50), fontSize: responsiveHeight(16)
    },
    textInputStyleBig: { height: responsiveHeight(80), fontSize: responsiveHeight(16), zIndex: 1 },
    salesStyle: {
        color: 'black',
        fontSize: responsiveHeight(18),
        fontWeight: '500'
    },
    canvas: {
        backgroundColor: colors.PrimaryGray,
        flex: 1,
        borderTopRightRadius: responsiveHeight(50),
        borderTopLeftRadius: responsiveHeight(50),
        // padding: responsiveHeight(30),
    },
    styleInformant: {
        borderWidth: 1,
        height: responsiveHeight(300),
        borderRadius: 10,
        padding: responsiveHeight(5),
        paddingHorizontal: responsiveHeight(20),
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    containerPd: {
        padding: responsiveHeight(15)
    },
    HeadingContainer: {
        marginTop: responsiveHeight(15),
        marginBottom: responsiveHeight(10),
        marginHorizontal: responsiveHeight(10)
    },
    headingStyle: {
        color: colors.PrimaryWhite,
        fontSize: responsiveHeight(26),
        fontWeight: 'bold'
    },
    containerImage: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    imageStyle: {
        height: responsiveHeight(50),
        width: responsiveHeight(50),
        resizeMode: 'contain'
    },
    menuStyle: {
        height: responsiveHeight(35),
        width: responsiveHeight(35),
        resizeMode: 'contain'
    },
    padss: { paddingHorizontal: responsiveHeight(30), flex: 1, paddingTop: responsiveHeight(30) },
    contBot: { backgroundColor: colors.PrimaryWhite, height: responsiveHeight(150), borderTopLeftRadius: 20, borderTopRightRadius: 20, marginTop: responsiveHeight(10), paddingHorizontal: responsiveHeight(10), paddingTop: responsiveHeight(10) }

})

export default Home