import { View, Text, Modal, StyleSheet, TouchableOpacity, TextInput } from 'react-native'
import React, { useState } from 'react'
import { responsiveHeight, responsiveWidth } from '../Helper/Responsive'
import ButtonPrimary from './ButtonPrimary'

const Modals = ({ visible, onpress }) => {

    const [ItemName, setItemName] = useState('')
    const [ItemPrice, setItemPrice] = useState('')

    return (
        <View>
            <Modal visible={visible} transparent>
                <View style={style.container}>
                    <View style={style.conWhite}>
                        <View style={style.textTittle}>
                            <Text>New Item</Text>

                        </View>
                        <View style={[style.textInputBarSm, {
                            marginHorizontal: responsiveHeight(30),
                            marginTop: responsiveHeight(80)
                        }]}>
                            <TextInput
                                placeholder='Item Name'
                                style={style.textInputStyleSM}
                                value={ItemName}
                                onChangeText={(e) => {
                                    setItemName(e)
                                }}
                            />
                        </View>
                        <View style={[style.textInputBarSm, {
                            marginHorizontal: responsiveHeight(30),
                            marginVertical: responsiveHeight(30)
                        }]}>
                            <TextInput
                                placeholder='Item price'
                                style={style.textInputStyleSM}
                                value={ItemPrice}
                                onChangeText={(e) => {
                                    setItemPrice(e)
                                }}
                            />
                        </View>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: responsiveHeight(10) }}>
                            <ButtonPrimary
                                Label={'Save'}
                                widths={100}
                                onpress={onpress} />
                            <View style={{ marginHorizontal: responsiveHeight(20), }} />
                            <ButtonPrimary
                                Label={'cancel'}
                                colorss={'white'}
                                widths={100}
                                bodco={'ada'}
                                onpress={onpress} />
                        </View>
                    </View>
                </View>
            </Modal>
        </View>
    )
}

const style = StyleSheet.create({
    container: {
        backgroundColor: 'rgba(33, 33, 33, 0.75)',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: responsiveHeight(24)
    },
    conWhite: {
        backgroundColor: 'white',
        height: responsiveHeight(480),
        width: responsiveWidth(340),
        borderRadius: 8,
    },
    textTittle: {
        marginTop: responsiveHeight(24),
        marginBottom: responsiveHeight(12),
        alignItems: 'center'
    },
    textInputBarSm: {
        borderWidth: 1,
        paddingHorizontal: responsiveHeight(21),
        justifyContent: 'center',
        borderRadius: 10,
    },
    textInputStyleSM: {
        height: responsiveHeight(50), fontSize: responsiveHeight(16)
    },


})
export default Modals