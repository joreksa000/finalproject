import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native'
import React, { useState } from 'react'
import { responsiveHeight } from '../Helper/Responsive'
import colors from '../Helper/colors'
import numberWithCommas from '../Helper/Formater'
import { useDispatch } from 'react-redux'
import { Deteles } from '../redux/Action/Action'

const pencils = 'https://cdn1.iconfinder.com/data/icons/web-design-29/60/pencil__icon__vector__line-512.png'
const bins = 'https://cdn-icons-png.flaticon.com/512/4812/4812459.png'
const Cards = ({ types, data, updates, deletes }) => {
    const [quans, setQuans] = useState(data?.Quantity)
    const [prices, setPrices] = useState(data?.Price)
    const [Totals, setTotals] = useState(data?.Price * data?.Quantity)
    const dispatch = useDispatch()
    function DELL(ids) {
        dispatch(Deteles(ids))
    }
    return (
        <View>
            {types == 'Sale' && types !== '' ?
                <View style={style.container}>
                    <View style={style.WrappedStyle}>
                        <View style={style.BlocksData}>
                            <View style={style.justip}>
                                <Text style={style.NAMES}>{data?.ItemName}</Text>
                                <Text>{numberWithCommas(prices)}</Text>
                            </View>
                            <View style={style.justip}>
                                <Text style={style.lbl2}>QTY</Text>
                                <View style={style.Quan}>
                                    <View>
                                        <TouchableOpacity style={style.nums} onPress={() => {
                                            if (quans !== 0 || quans == 0) {
                                                setQuans(quans + 1)
                                                setTotals(Totals + data.Price)
                                            }
                                        }}>
                                            <Text>+</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <Text>{quans}</Text>
                                    <View>
                                        <TouchableOpacity style={style.nums} onPress={() => {
                                            if (quans !== 0) {
                                                setQuans(quans - 1)
                                                setTotals(Totals - data.Price)
                                            }

                                        }}>
                                            <Text>-</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                            <View style={style.justip}>
                                <Text style={style.lbl2}>Total</Text>
                                <Text>{numberWithCommas(Totals)}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={style.wrappedButton}>
                        <TouchableOpacity onPress={updates}>
                            <Image source={{ uri: pencils }} style={style.edits} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => {
                            DELL(data?.ItemId)
                        }}>
                            <Image source={{ uri: bins }} style={style.trash} />
                        </TouchableOpacity>
                    </View>
                </View>
                :
                <>

                </>
            }
        </View >
    )
}


const style = StyleSheet.create({
    container: {
        backgroundColor: colors.PrimaryWhite,
        elevation: 5,
        height: responsiveHeight(78),
        borderRadius: 10,
        paddingLeft: responsiveHeight(15),
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    wrappedButton: {
        width: responsiveHeight(76),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginLeft: responsiveHeight(5)
    },
    WrappedStyle: {
        justifyContent: 'center',
        flex: 1,
        marginHorizontal: responsiveHeight(10)
    },
    Quan: {
        marginTop: responsiveHeight(2),
        backgroundColor: colors.PrimaryGray,
        flexDirection: 'row',
        width: responsiveHeight(100),
        justifyContent: 'space-between',
        padding: responsiveHeight(2),
        borderRadius: 10
    },
    BlocksData: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    nums: {
        backgroundColor: colors.PrimaryWhite,
        width: responsiveHeight(30),
        alignItems: 'center',
        height: responsiveHeight(30),
        borderRadius: 10
    },
    justip: {
        alignItems: 'center',
        justifyContent: 'space-between',
        height: responsiveHeight(60)
    },
    trash: { height: responsiveHeight(45), width: responsiveHeight(45), resizeMode: 'contain' },
    edits: { height: responsiveHeight(30), width: responsiveHeight(30), resizeMode: 'contain' },
    lbl2: {
        color: 'black',
        fontSize: responsiveHeight(15),
        fontWeight: '500'
    },
    NAMES: {
        color: 'black',
        fontSize: responsiveHeight(18),
        fontWeight: '700'
    }

})
export default Cards