
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import React from 'react'
import { responsiveHeight } from '../Helper/Responsive'
import colors from '../Helper/colors'

const ButtonPrimary = ({ Label, onpress, widths, colorss }) => {
    return (
        <View>
            <TouchableOpacity
                style={styles.container(colorss, widths)}
                onPress={onpress}>
                <Text style={styles.lbStyle(colorss)}>{Label}</Text>
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    container: (x, r) => ({
        backgroundColor: x ? x : colors.Primary,
        height: responsiveHeight(35),
        width: r ? r : responsiveHeight(100),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        borderWidth: x ? 1 : null,
        borderColor: x ? '#6A7622' : null
    }),
    lbStyle: (x) => ({
        color: x ? '#6A7622' : colors.PrimaryWhite,
        fontSize: responsiveHeight(15),
        fontWeight: '500'
    })
})

export default ButtonPrimary