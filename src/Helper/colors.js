export default colors = {
    Primary: '#6A7622',
    PrimaryWhite: '#fff',
    PrimaryGray: '#eeeeee'
}